(function($){
  $(function(){

    $('.button-collapse').sideNav();
    $('.modal').modal({
        dismissible: true,
        opacity: .5,
        inDuration: 150, // Transition in duration
        outDuration: 150, // Transition out duration
    });

    $('.back').click(function(){
		parent.history.back();
		return false;
	});

    $('#input-search').on('click',function() {

    	$('.search-box').addClass('search-box--searchform')
    	$('.search-results').removeClass('hide');

    	$(this).keyup(function(event) {
    		$('.search-results-data').removeClass('hide');
    	})
    });

    var deleteGenre = function() {
        var $elem = $(this);
        var $parent = $elem.parent();
        var genre = $elem.prev().html();
        $parent.hide(150, function() {
            $parent.remove();
        });
        $('#genres-list').find('li').filter(function () {
            return $(this).text() === genre;
        }).prop('hidden', false);
    };

    $('.trashcan').on('click', deleteGenre);

    $('#genres-list').find('li').on('click', function() {
        var $item = $(this);
        var genre = $item.html();
        var $added = $('<div style="display: none" class="settings-genre">' + genre + '<a class="trashcan"><svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 268.476 268.476"><path></path></svg></a></div>');
        $added.find('.trashcan').on('click', deleteGenre);
        $('#preffered-genres').append($added);
        $added.show(150);
        $('#genres-modal').modal('close');
        $item.prop('hidden', true);
    });

    $('#login').on('click', function() {
        $(this).html('Přihlášen jako <strong>Pepa Novák</strong> přes FB');
        $('#logout').show();
    });

    $('#logout').on('click', function() {
        $('#login').html('Přihlásit se');
        $(this).hide();
    });

    $('.btn-cal-evt').on('click', function() {
        var btn = $(this);
        btn.html('Uloženo');
        btn.addClass('disabled');
    });

    $('.btn-share').on('click', function() {
        var btn = $(this);
        btn.html('Sdíleno');
        btn.addClass('disabled');
    });

    $('.close-text').on('click',function(){
    	if ( $('.search-box').hasClass('search-box--searchform') ) {
    		$('.search-box').removeClass('search-box--searchform');
    		$('.search-results').addClass('hide');
    		$('.search-results-data').addClass('hide');
    	}
    });

    $('.type-movies').on('click',function(){
    	$('.type-cinemas').removeClass('active');
    	$('.type-cinemas-list').hide();
    	$('.type-movies-list').show();
    	$('.type-movies').addClass('active');
    });

    $('.type-cinemas').on('click',function(){
    	$('.type-cinemas').addClass('active');
    	$('.type-cinemas-list').show();
    	$('.type-movies-list').hide();
    	$('.type-movies').removeClass('active');
    });

    $('.type-list').on('click',function(){
    	$('.type-map').removeClass('active');
    	$('.container-map').hide();
    	$('.type-list-list').show();
    	$('.type-list').addClass('active');
    });

    $('.type-map').on('click',function(){
    	$('.type-map').addClass('active');
    	$('.container-map').show();
    	$('.type-list-list').hide();
    	$('.type-list').removeClass('active');
    });

  }); // end of document ready
})(jQuery); // end of jQuery name space