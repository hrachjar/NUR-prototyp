module.exports = function (grunt) {
    require('jit-grunt')(grunt);
    grunt.initConfig({
        less: {
            development: {
                files: {
                    "css/style.css": "less/style.less" // destination file and source file
                }
            }
        },
        autoprefixer: {
            options: {
                //browsers: ['last 8 versions']
                browsers: ['> 0.05% in CZ']
            },
            dist: {
                files: {
                    'css/style.css': 'css/style.css'
                }
            }
        },
        cssmin: {
            dist: {
                files: {
                    'css/style.min.css': ['css/style.css']
                }
            }
        },
        watch: {
            styles: {
                files: ['less/**/*.less'], // which files to watch
                tasks: ['less', 'autoprefixer', 'cssmin'],
                options: {
                    nospawn: true
                }
            },
            npm: {
                files: 'package.json',
                tasks: ['npm-install']
            }
        }
    });
    grunt.registerTask('default', ['develop']);
    grunt.registerTask('develop', ['less', 'autoprefixer', 'cssmin', 'watch']);
};